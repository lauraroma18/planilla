-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-09-2020 a las 21:03:09
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: 'gpctplanilla'
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'conductor'
--

CREATE TABLE 'conductor' (
  'cedula' varchar(15) NOT NULL,
  'nombre_completo' varchar(100) NOT NULL,
  'sexo' varchar(10) NOT NULL,
  'fecha_nacimiento' date NOT NULL,
  'telefono' varchar(45) NOT NULL,
  'direccion' varchar(50) NOT NULL,
  'correo' varchar(50) NOT NULL,
  'talla' varchar(4) DEFAULT NULL,
  'peso' varchar(4) DEFAULT NULL,
  'cargo' varchar(45) NOT NULL,
  'seguridad_social' varchar(45) DEFAULT NULL,
  'caja_compensacion' varchar(45) DEFAULT NULL,
  'estado_civil' varchar(45) DEFAULT NULL,
  'pension' varchar(45) DEFAULT NULL,
  'arl' varchar(45) DEFAULT NULL,
  'licencia_conduccion' date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'conductor'
--

INSERT INTO 'conductor' ('cedula', 'nombre_completo', 'sexo', 'fecha_nacimiento', 'telefono', 'direccion', 'correo', 'talla', 'peso', 'cargo', 'seguridad_social', 'caja_compensacion', 'estado_civil', 'pension', 'arl', 'licencia_conduccion') VALUES
('1110780524', 'Mario Santana Rey', 'm', '1980-12-08', '3004568923', 'cll 103 # 45 - 202', 'marios@hotmail.com', '1.79', '95', 'Conductor', 'Nueva eps', 'Comfandi', 'Casado', 'Colpensiones', 'Sura', '2021-08-20'),
('1119865691', 'David Gerónimo Perez Arco', 'm', '1989-09-30', '3174560055', 'cra 12 # 25- 03', 'darco@gmail.com', '1.98', '91', 'Conductor', 'Medimas', 'Comfandi', 'Soltero', 'Porvenir', 'Sura', '2021-08-20'),
('123457000', 'fdfghfd', 'fd', '0000-00-00', 'bfbbd', 'ffdfb', 'bfffd', 'bb', 'bb', 'bffdb', 'bdffdf', 'fdfbf', 'bdf', 'ffdfdbf', 'bfdfbf', '2020-12-25'),
('66780524', 'Roberto García Paredes', 'm', '1985-12-10', '3174568923', 'cll 12 # 45 - 09', 'rogapa@hotmail.com', '1.85', '85', 'Conductor', 'Nueva eps', 'Comfandi', 'Casado', 'Colpensiones', 'Sura', '2021-02-20'),
('94865600', 'Javier Alonso Perez Arco', 'm', '1989-09-30', '3174560055', 'cra 12 # 25- 03', 'jalonso@gmail.com', '1.98', '91', 'Conductor', 'Medimas', 'Comfandi', 'Soltero', 'Porvenir', 'Sura', '2021-08-20'),
('94865691', 'Santiado Vera Arco', 'm', '0000-00-00', '3020050055', 'cra 48 # 96- 03', 'veraco@gmail.com', '1.78', '85', 'Conductor', 'Medimas', 'Comfandi', 'Soltero', 'Porvenir', 'Sura', '2021-08-20');

--
-- Disparadores 'conductor'
--
DELIMITER $$
CREATE TRIGGER 'Conductor_AFTER_DELETE' AFTER DELETE ON 'conductor' FOR EACH ROW INSERT INTO Ex_empleado(cedula, nombre_completo, sexo, fecha_nacimiento, telefono, direccion, correo, cargo, seguridad_social, caja_compensacion, estado_civil, pension, arl) VALUES(old.cedula, old.nombre_completo, old.sexo, old.fecha_nacimiento, old.telefono, old.direccion, old.correo, old.cargo, old.seguridad_social, old.caja_compensacion, old.estado_civil, old.pension, old.arl)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'conductor_vehiculo'
--

CREATE TABLE 'conductor_vehiculo' (
  'id' int(11) NOT NULL,
  'id_placa' varchar(6) NOT NULL,
  'id_conductor' varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'conductor_vehiculo'
--

INSERT INTO 'conductor_vehiculo' ('id', 'id_placa', 'id_conductor') VALUES
(1, 'HKN105', '66780524'),
(2, 'HKN105', '94865600'),
(3, 'HKN123', '94865691'),
(4, 'QWR008', '1110780524'),
(5, 'QWR857', '1119865691');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'contratante'
--

CREATE TABLE 'contratante' (
  'nombre_completo' varchar(100) NOT NULL,
  'telefono' varchar(45) NOT NULL,
  'direccion' varchar(50) NOT NULL,
  'correo' varchar(50) NOT NULL,
  'cedula' varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'contratante'
--

INSERT INTO 'contratante' ('nombre_completo', 'telefono', 'direccion', 'correo', 'cedula') VALUES
('Dario Hinojosa', '300568923', 'cra 24 # 103 - 80', 'dahi@hotmail.com', '1113002005'),
('Daniela Hinojosa', '300568923', 'cra 15 # 208 - 09', 'danisa@hotmail.com', '1116008777'),
('Carolina Quiceno', '300568008', 'cll 15 # 103 - 09', 'carolaqui@hotmail.com', '1117902005'),
('Milena Castro', '300568920', 'cra 65 # 103 - 09', 'camitro@hotmail.com', '1118702005'),
('Ramiro Benitez', '3174568923', 'cll 12 # 45 - 09', 'benitora@hotmail.com', '668725106');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'empleado'
--

CREATE TABLE 'empleado' (
  'cedula' varchar(20) NOT NULL,
  'nombre_completo' varchar(100) NOT NULL,
  'sexo' varchar(10) NOT NULL,
  'fecha_nacimiento' date NOT NULL,
  'telefono' varchar(45) NOT NULL,
  'direccion' varchar(50) NOT NULL,
  'correo' varchar(50) NOT NULL,
  'talla' varchar(4) DEFAULT NULL,
  'peso' varchar(4) DEFAULT NULL,
  'cargo' varchar(45) NOT NULL,
  'seguridad_social' varchar(45) DEFAULT NULL,
  'caja_compensacion' varchar(45) DEFAULT NULL,
  'estado_civil' varchar(45) DEFAULT NULL,
  'pension' varchar(45) DEFAULT NULL,
  'arl' varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'empleado'
--

INSERT INTO 'empleado' ('cedula', 'nombre_completo', 'sexo', 'fecha_nacimiento', 'telefono', 'direccion', 'correo', 'talla', 'peso', 'cargo', 'seguridad_social', 'caja_compensacion', 'estado_civil', 'pension', 'arl') VALUES
('1102691004', 'Viviana Perez', 'f', '1987-07-10', '3170068923', 'cll 85 # 75 - 09', 'vvperez@hotmail.com', '1.69', '63', 'Secretaria', 'Medimas', 'Comfandi', 'Soltero', 'Colpensiones', 'Sura'),
('1102997004', 'Danilo Casas', 'Masculino', '1987-07-20', '3170068823', 'cll 05 # 75 - 09', 'casasdani@hotmail.com', '1.79', '68', 'Jefe de Operación', 'Medimas', 'Comfandi', 'Soltero', 'Colpensiones', 'Sura'),
('1117865691', 'Milena Saavedra', 'f', '1980-07-10', '3174568923', 'cll 12 # 45 - 09', 'milenasaav@hotmail.com', '1.69', '63', 'Secretaria', 'Medimas', 'Comfandi', 'Soltero', 'Colpensiones', 'Sura'),
('1158960008', 'Rafael Toro', 'm', '1987-07-10', '3170068923', 'cll 85 # 12 - 09', 'rafap@hotmail.com', '1.89', '83', 'Jefe de Operación', 'Medimas', 'Comfandi', 'Soltero', 'Colpensiones', 'Sura'),
('1158960748', 'Rafael Perez', 'm', '1987-07-10', '3170068923', 'cll 85 # 12 - 09', 'rafap@hotmail.com', '1.89', '83', 'Jefe de Operación', 'Medimas', 'Comfandi', 'Soltero', 'Colpensiones', 'Sura'),
('1234567890', 'prueba normal', 'f', '1980-07-10', '3174568923', 'cll 12 # 45 - 09', 'test@zhotmail.com', '1.85', '91', 'Jefe Operación', 'Nueva eps', 'Comfandi', 'Casado', 'Colpensiones', 'Sura');

--
-- Disparadores 'empleado'
--
DELIMITER $$
CREATE TRIGGER 'Empleado_AFTER_DELETE' AFTER DELETE ON 'empleado' FOR EACH ROW INSERT INTO Ex_empleado(cedula, nombre_completo, sexo, fecha_nacimiento, telefono, direccion, correo, cargo, seguridad_social, caja_compensacion, estado_civil, pension, arl) VALUES(old.cedula, old.nombre_completo, old.sexo, old.fecha_nacimiento, old.telefono, old.direccion, old.correo, old.cargo, old.seguridad_social, old.caja_compensacion, old.estado_civil, old.pension, old.arl)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'ex_empleado'
--

CREATE TABLE 'ex_empleado' (
  'cedula' varchar(20) NOT NULL,
  'nombre_completo' varchar(100) NOT NULL,
  'sexo' varchar(10) NOT NULL,
  'fecha_nacimiento' date NOT NULL,
  'telefono' varchar(45) NOT NULL,
  'direccion' varchar(50) NOT NULL,
  'correo' varchar(50) NOT NULL,
  'cargo' varchar(45) NOT NULL,
  'seguridad_social' varchar(45) DEFAULT NULL,
  'caja_compensacion' varchar(45) DEFAULT NULL,
  'estado_civil' varchar(45) DEFAULT NULL,
  'pension' varchar(45) DEFAULT NULL,
  'arl' varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'ex_empleado'
--

INSERT INTO 'ex_empleado' ('cedula', 'nombre_completo', 'sexo', 'fecha_nacimiento', 'telefono', 'direccion', 'correo', 'cargo', 'seguridad_social', 'caja_compensacion', 'estado_civil', 'pension', 'arl') VALUES
('1114587902', 'Ranses Giraldo', 'Masculino', '2020-09-01', 'dfffdf', 'gdfgfdg', 'dfggfg', 'Conductor', 'fgfgf', 'fdgfgfd', 'Soltero', 'fgfg', 'dggd'),
('1115003558', 'Luis Delgado', 'f', '1994-04-13', '3174568923', 'cra 12 # 25- 03', 'luisdelg@hotmail.com', 'Conductor', 'Nueva eps', 'Comfandi', 'Casado', 'Porvenir', 'Sura'),
('1117865000', 'Victor Marin', 'm', '1989-09-30', '3174568923', 'cll 12 # 45 - 09', 'darco@gmail.com', 'Conductor', 'Nueva eps', 'Comfandi', 'Casado', 'Colpensiones', 'Sura'),
('1254809', 'Federico Rincón Velez', 'Masculino', '1998-11-11', '2321415', 'cll 45 89-09', 'rive@hotmail.com', 'Jefe Operación', 'nbjkdf', 'fdddff', 'Femenino', 'fddfdf', 'ddffdfd'),
('567', 'fghfhfhg', 'm', '1985-12-10', '3174568923', 'cll 12 # 45 - 09', 'rafaelaV@gmail.com', 'Conductor', 'Nueva eps', 'Comfandi', 'Casado', 'Colpensiones', 'Sura'),
('63123008', 'Luisa Rengifo', 'f', '1998-09-30', '3174560055', 'cra 12 # 25- 58', 'renlu@gmail.com', 'Secretaria', 'Medimas', 'Comfandi', 'Soltero', 'Colpensiones', 'Sura'),
('64580235', 'Marcela León', 'f', '1995-09-08', '3174568923', 'cll 12 # 45 - 09', 'leon@gmail.com', 'Secretaria', 'Medimas', 'Comfandi', 'Soltero', 'Colpensiones', 'Sura'),
('66780000', 'Nora Tangarife', 'm', '1980-07-10', '3174560055', 'cll 12 # 45 - 09', 'benitora@hotmail.com', 'Secretaria', 'Nueva eps', 'Comfandi', 'Casado', 'Colpensiones', 'Sura');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'listachequeos'
--

CREATE TABLE 'listachequeos' (
  'id' int(11) NOT NULL,
  'placa' varchar(6) NOT NULL,
  'id_conductor' varchar(20) NOT NULL,
  'fecha' date NOT NULL,
  'nivel_aceite' varchar(1) NOT NULL,
  'fugas_refrigeracion' varchar(1) NOT NULL,
  'nivel_refrigerante' varchar(1) NOT NULL,
  'agua_parabrisas' varchar(1) NOT NULL,
  'juntas_cardianicas' varchar(1) NOT NULL,
  'liquido_embrague' varchar(1) NOT NULL,
  'drenaje_tanques' varchar(1) NOT NULL,
  'liquido_frenos' varchar(1) NOT NULL,
  'fugas_caja_direccion' varchar(1) NOT NULL,
  'pastilla_frenos' varchar(1) NOT NULL,
  'liquido_direccion' varchar(1) NOT NULL,
  'pitol' varchar(1) NOT NULL,
  'sistema_aire' varchar(1) NOT NULL,
  'sistema_limpiabrisas' varchar(1) NOT NULL,
  'luces_alta_bajas' varchar(1) NOT NULL,
  'direccionales' varchar(1) NOT NULL,
  'exploradoras' varchar(1) NOT NULL,
  'luces_estacionamiento' varchar(1) NOT NULL,
  'reversa' varchar(1) NOT NULL,
  'luces_freno' varchar(1) NOT NULL,
  'luces_interiores' varchar(1) NOT NULL,
  'estado_respuesto_llantas' varchar(1) NOT NULL,
  'perfil_llantas' varchar(1) NOT NULL,
  'calibracion_llantas' varchar(1) NOT NULL,
  'estado_espejos' varchar(1) NOT NULL,
  'martillo_seguridad' varchar(1) NOT NULL,
  'poliza_contractual' varchar(1) NOT NULL,
  'poliza_extracontractual' varchar(1) NOT NULL,
  'tarjeta_operacion' varchar(1) NOT NULL,
  'rtm' varchar(1) NOT NULL,
  'licencia_transito' varchar(1) NOT NULL,
  'licencia_conduccion' varchar(1) NOT NULL,
  'soat' varchar(1) NOT NULL,
  'extintor' varchar(1) NOT NULL,
  'botiquin' varchar(1) NOT NULL,
  'herramientas' varchar(1) NOT NULL,
  'conos_reflectivos' varchar(1) NOT NULL,
  'chaleco' varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'listachequeos'
--

INSERT INTO 'listachequeos' ('id', 'placa', 'id_conductor', 'fecha', 'nivel_aceite', 'fugas_refrigeracion', 'nivel_refrigerante', 'agua_parabrisas', 'juntas_cardianicas', 'liquido_embrague', 'drenaje_tanques', 'liquido_frenos', 'fugas_caja_direccion', 'pastilla_frenos', 'liquido_direccion', 'pitol', 'sistema_aire', 'sistema_limpiabrisas', 'luces_alta_bajas', 'direccionales', 'exploradoras', 'luces_estacionamiento', 'reversa', 'luces_freno', 'luces_interiores', 'estado_respuesto_llantas', 'perfil_llantas', 'calibracion_llantas', 'estado_espejos', 'martillo_seguridad', 'poliza_contractual', 'poliza_extracontractual', 'tarjeta_operacion', 'rtm', 'licencia_transito', 'licencia_conduccion', 'soat', 'extintor', 'botiquin', 'herramientas', 'conos_reflectivos', 'chaleco') VALUES
(1, 'HKN105', '1110780524', '2020-08-02', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'),
(6, 'HKN123', '1110780524', '2020-06-15', 'B', 'B', 'B', 'B', 'B', 'B', 'R', 'R', 'R', 'R', 'B', 'B', 'B', 'B', 'B', 'B', 'R', 'R', 'R', 'B', 'R', 'B', 'R', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'),
(7, 'QWR008', '66780524', '2020-08-31', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'R', 'R', 'R', 'B'),
(8, 'QWR857', '66780524', '2020-09-23', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'),
(11, 'HKN105', '1119865691', '2020-09-11', 'B', 'B', 'B', 'B', 'R', 'R', 'R', 'R', 'R', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'planilla'
--

CREATE TABLE 'planilla' (
  'id' int(11) NOT NULL,
  'fecha_inicio' date NOT NULL,
  'fecha_final' date NOT NULL,
  'placa' varchar(45) NOT NULL,
  'origen' varchar(50) NOT NULL,
  'destino' varchar(50) NOT NULL,
  'id_contratante' varchar(20) NOT NULL,
  'objeto_contrato' varchar(100) NOT NULL,
  'Anulada' varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'planilla'
--

INSERT INTO 'planilla' ('id', 'fecha_inicio', 'fecha_final', 'placa', 'origen', 'destino', 'id_contratante', 'objeto_contrato', 'Anulada') VALUES
(1, '2020-09-25', '2020-09-30', 'HKN105', 'Tuluá', 'Barranquilla', '1117902005', 'Vacacional', 'No'),
(2, '2020-10-25', '2020-10-30', 'HKN123', 'Tuluá', 'Cartagena', '1116008777', 'Empresarial', 'Si'),
(3, '2020-12-17', '2020-12-22', 'QWR008', 'Tuluá', 'Medellín', '1113002005', 'Empresarial', 'No'),
(4, '2020-12-17', '2020-12-19', 'QWR857', 'Tuluá', 'Cali', '1118702005', 'Vacional', 'No'),
(5, '2020-12-17', '2020-12-19', 'QWR857', 'Tuluá', 'Pereira', '668725106', 'Deportivo', 'Si'),
(6, '2020-09-03', '2020-09-19', 'QWR857', 'Tuluá', 'Bogotá', '1117902005', 'Vacacional', 'No');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'propietario'
--

CREATE TABLE 'propietario' (
  'nombre_completo' varchar(100) NOT NULL,
  'fecha_nacimiento' date NOT NULL,
  'telefono' varchar(45) NOT NULL,
  'direccion' varchar(50) NOT NULL,
  'correo' varchar(50) NOT NULL,
  'activo' varchar(2) NOT NULL,
  'cedula' varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'propietario'
--

INSERT INTO 'propietario' ('nombre_completo', 'fecha_nacimiento', 'telefono', 'direccion', 'correo', 'activo', 'cedula') VALUES
('Camilo Viera', '1993-07-16', '3104568923', 'cra 12 # 45 - 09', 'camilolaV@gmail.com', 'Si', '1105261098'),
('Carlos Berrio', '1979-09-02', '3120068923', 'cra 12 # 30 - 14', 'berrio.carlosV@gmail.com', 'No', '1116005003'),
('Rafaela Viera', '1956-09-08', '3174568923', 'cll 12 # 45 - 09', 'rafaelaV@gmail.com', 'Si', '1116261098'),
('Carlos Rivera', '1979-09-08', '3171168923', 'cll 12 # 30 - 09', 'rivera.carlosV@gmail.com', 'Si', '1116695003'),
('Roberta Velez', '1956-09-08', '3174568923', 'cll 150 # 45 - 09', 'robeveV@gmail.com', 'Si', '1119261098');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'usuariosconductores'
--

CREATE TABLE 'usuariosconductores' (
  'cedula' varchar(20) NOT NULL,
  'pass' varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'usuariosconductores'
--

INSERT INTO 'usuariosconductores' ('cedula', 'pass') VALUES
('1110780524', '12345'),
('1119865691', '12345'),
('123457000', '12345'),
('66780524', '12345'),
('94865600', '12345'),
('94865691', '12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'usuariosempleados'
--

CREATE TABLE 'usuariosempleados' (
  'cedula' varchar(20) NOT NULL,
  'pass' varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'usuariosempleados'
--

INSERT INTO 'usuariosempleados' ('cedula', 'pass') VALUES
('1102691004', '12345'),
('1102997004', '12345'),
('1117865691', '12345'),
('1158960008', '12345'),
('1158960748', '12345'),
('1234567890', '0000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'vehiculo'
--

CREATE TABLE 'vehiculo' (
  'placa' varchar(6) NOT NULL,
  'tipo' varchar(45) NOT NULL,
  'marca' varchar(45) NOT NULL,
  'num_interno' varchar(4) NOT NULL,
  'modelo' varchar(4) NOT NULL,
  'combustible' varchar(10) NOT NULL,
  'servicio' varchar(45) NOT NULL,
  'licencia_transito' varchar(20) NOT NULL,
  'tarjeta_operacion' date NOT NULL,
  'soat' date NOT NULL,
  'poliza_rc_rce' date NOT NULL,
  'tecnomecanica' date NOT NULL,
  'id_propietario' varchar(20) NOT NULL,
  'afiliado' varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla 'vehiculo'
--

INSERT INTO 'vehiculo' ('placa', 'tipo', 'marca', 'num_interno', 'modelo', 'combustible', 'servicio', 'licencia_transito', 'tarjeta_operacion', 'soat', 'poliza_rc_rce', 'tecnomecanica', 'id_propietario', 'afiliado') VALUES
('HJD178', 'Bus', 'Nissan', '005', '2013', 'ACPM', 'Público especiales y de turismo ', '30075242', '2020-12-25', '2021-08-18', '2021-12-04', '2021-09-04', '1119261098', 'No'),
('HKN105', 'Bus', 'Nissan', '002', '2013', 'ACPM', 'Público especiales y de turismo ', '34355241', '2020-12-25', '2021-06-18', '2021-02-12', '2021-09-04', '1116005003', 'Si'),
('HKN123', 'Bus', 'Chevrolet', '001', '2016', 'ACP', 'Público especiales y de turismo ', '34355242', '2020-12-25', '2021-06-18', '2021-02-12', '2021-09-15', '1105261098', 'Si'),
('QWR008', 'Bus', 'Chevrolet', '003', '2009', 'ACPM', 'Público especiales y de turismo ', '34355004', '2020-12-27', '2021-08-18', '2021-01-12', '2021-09-04', '1116695003', 'Si'),
('QWR857', 'Bus', 'Nissan', '004', '2016', 'ACPM', 'Público especiales y de turismo ', '34355240', '2020-12-25', '2021-08-20', '2021-12-04', '2021-09-17', '1116261098', 'Si');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla 'conductor'
--
ALTER TABLE 'conductor'
  ADD PRIMARY KEY ('cedula');

--
-- Indices de la tabla 'conductor_vehiculo'
--
ALTER TABLE 'conductor_vehiculo'
  ADD PRIMARY KEY ('id'),
  ADD KEY 'id_placa' ('id_placa'),
  ADD KEY 'id_conductor' ('id_conductor');

--
-- Indices de la tabla 'contratante'
--
ALTER TABLE 'contratante'
  ADD PRIMARY KEY ('cedula');

--
-- Indices de la tabla 'empleado'
--
ALTER TABLE 'empleado'
  ADD PRIMARY KEY ('cedula');

--
-- Indices de la tabla 'ex_empleado'
--
ALTER TABLE 'ex_empleado'
  ADD PRIMARY KEY ('cedula');

--
-- Indices de la tabla 'listachequeos'
--
ALTER TABLE 'listachequeos'
  ADD PRIMARY KEY ('id'),
  ADD KEY 'id_conductor' ('id_conductor'),
  ADD KEY 'placa' ('placa');

--
-- Indices de la tabla 'planilla'
--
ALTER TABLE 'planilla'
  ADD PRIMARY KEY ('id'),
  ADD KEY 'placa' ('placa'),
  ADD KEY 'id_contratante' ('id_contratante');

--
-- Indices de la tabla 'propietario'
--
ALTER TABLE 'propietario'
  ADD PRIMARY KEY ('cedula');

--
-- Indices de la tabla 'usuariosconductores'
--
ALTER TABLE 'usuariosconductores'
  ADD PRIMARY KEY ('cedula');

--
-- Indices de la tabla 'usuariosempleados'
--
ALTER TABLE 'usuariosempleados'
  ADD PRIMARY KEY ('cedula');

--
-- Indices de la tabla 'vehiculo'
--
ALTER TABLE 'vehiculo'
  ADD PRIMARY KEY ('placa'),
  ADD KEY 'id_propietario' ('id_propietario');

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla 'conductor_vehiculo'
--
ALTER TABLE 'conductor_vehiculo'
  MODIFY 'id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla 'listachequeos'
--
ALTER TABLE 'listachequeos'
  MODIFY 'id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla 'planilla'
--
ALTER TABLE 'planilla'
  MODIFY 'id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla 'conductor_vehiculo'
--
ALTER TABLE 'conductor_vehiculo'
  ADD CONSTRAINT 'conductor_vehiculo_ibfk_1' FOREIGN KEY ('id_placa') REFERENCES 'vehiculo' ('placa'),
  ADD CONSTRAINT 'conductor_vehiculo_ibfk_2' FOREIGN KEY ('id_conductor') REFERENCES 'conductor' ('cedula');

--
-- Filtros para la tabla 'listachequeos'
--
ALTER TABLE 'listachequeos'
  ADD CONSTRAINT 'listachequeos_ibfk_1' FOREIGN KEY ('id_conductor') REFERENCES 'conductor' ('cedula'),
  ADD CONSTRAINT 'listachequeos_ibfk_2' FOREIGN KEY ('placa') REFERENCES 'vehiculo' ('placa');

--
-- Filtros para la tabla 'planilla'
--
ALTER TABLE 'planilla'
  ADD CONSTRAINT 'planilla_ibfk_1' FOREIGN KEY ('placa') REFERENCES 'vehiculo' ('placa'),
  ADD CONSTRAINT 'planilla_ibfk_2' FOREIGN KEY ('id_contratante') REFERENCES 'contratante' ('cedula');

--
-- Filtros para la tabla 'usuariosconductores'
--
ALTER TABLE 'usuariosconductores'
  ADD CONSTRAINT 'usuariosconductores_ibfk_1' FOREIGN KEY ('cedula') REFERENCES 'conductor' ('cedula');

--
-- Filtros para la tabla 'usuariosempleados'
--
ALTER TABLE 'usuariosempleados'
  ADD CONSTRAINT 'usuariosempleados_ibfk_1' FOREIGN KEY ('cedula') REFERENCES 'empleado' ('cedula');

--
-- Filtros para la tabla 'vehiculo'
--
ALTER TABLE 'vehiculo'
  ADD CONSTRAINT 'vehiculo_ibfk_1' FOREIGN KEY ('id_propietario') REFERENCES 'propietario' ('cedula');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package Conexión;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author lenovo
 */
public class Conexion {

    private static Connection con;
    //se debe agregar a la url lo de la zona horaria 
    private static final String URL = "jdbc:mysql://localhost:3306/gpctplanilla?serverTimezone=GMT-5";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";

    public Conexion() {
        //la conexión inicializamos  con null .
        con = null;

        // se construye un try catch porque es posible que no se establezca la conexion y necesitamos
        //que nuuestro aplicativo siga funcionando  asi no pueda hacer operaciones con la bd
        try {
            Class.forName(DRIVER);
            //creo la conexión a la bd
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            //para verificar que si establecí la conexión
            if (con != null) {
                System.out.println("Conexión Exitosa");
            }

        } catch (ClassNotFoundException | SQLException | HeadlessException e) {
            System.out.println("NO HAY CONEXIÓN --> " + e);
        }

    }

    // retorna la conexión

    public Connection getConection() {
        return con;
    }

    public void desconectar() {
        con = null;
        //comprobamos que se desconecto la conexión
        if (con == null) {
            System.out.println("Conexión Finalizada.");
        }
    }

}
